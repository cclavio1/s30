const express = require('express')
const mongoose = require('mongoose')
const port = 3000;
const app = express();
app.use(express.json())
app.use(express.urlencoded())
const dotenv = require('dotenv')
dotenv.config();
mongoose.connect(process.env.MONGO)
const db = mongoose.connection;
db.on('error', console.error.bind(console,'connection error:'))
db.once('open',()=>console.log(`Connected to Database`))

const user = mongoose.Schema({
    username:{
        type:String,
        required:[true,`Username is required`]
    },
    password:{
        type:String,
        required:[true,`Password is required`]
    }
})
const User = new mongoose.model("user",user)

app.post('/signup',(req,res)=>{
    let {username,password} = req.body;
    
    if(username==''||password==''){
        res.send("error: doublecheck data")
    }else{
        User.findOne({
            username:req.body.username
        }).then((result,err)=>{
            if(result===null){
                User.create({
                    username:req.body.username,
                    password:req.body.password
                })
                res.send("New user registered")
            }else{
                res.send("Duplicate username found")
            }
        })
 
    }
})


app.listen(port,()=>{
    console.log(`Connected to port ${port}`)
})